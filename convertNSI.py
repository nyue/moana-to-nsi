#!/usr/bin/python

import sys
import os
for dl in ('DELIGHT_ROOT', 'DELIGHT' ):
    dlp = os.getenv(dl)
    if dlp:
        sys.path.append(os.path.join(dlp, 'python'))
import nsi
import json
import math
import re
import argparse
import fnmatch

def vec_sub(v0, v1):
    return (v0[0] - v1[0], v0[1] - v1[1], v0[2] - v1[2])

def vec_add(v0, v1):
    return (v0[0] + v1[0], v0[1] + v1[1], v0[2] + v1[2])

def vec_dot(v0, v1):
    return (v0[0] * v1[0], v0[1] * v1[1], v0[2] * v1[2])

def vec_cross(v0, v1):
    return (
        v0[1] * v1[2] - v0[2] * v1[1],
        v0[2] * v1[0] - v0[0] * v1[2],
        v0[0] * v1[1] - v0[1] * v1[0])

def vec_normalize(v):
    n = vec_dot(v, v)
    f = 1.0 / math.sqrt(n[0] + n[1] + n[2])
    return (v[0] * f, v[1] * f, v[2] * f)

class MaterialBinder:
    """
    Class to record material bindings from a materials.json file and later
    apply them.
    """
    def __init__(self):
        # List of (pattern, material) tuples.
        self._pattern_bindings = []
        # name -> material dict for names without pattern matching. Some
        # materials (eg. coral) have fairly long assignment lists we don't want
        # to search linearly.
        self._direct_bindings = {}
        # material -> [list of (name, value)] dict for textured parameters of
        # the material
        self._textures = {}

    def apply(self, ctx, geoname, geopath, handle):
        """
        Applies material binding to an object.
        ctx
            The NSI context to produce the required commands to.
        geoname
            The geometry name. Used for binding purposes as well as for texture
            name generation.
        geopath
            The geometry path (from original scene hierarchy). Used as a
            fallback for binding purposes.
        handle
            The handle of the NSI node to bind to.
        """
        # Try the geometry name first
        if self._apply_to_name(ctx, geoname, handle, geoname):
            return
        # Use the full path. Reverse to match from bottom to top of path.
        for name in geopath.split('|')[::-1]:
            if self._apply_to_name(ctx, name, handle, geoname):
                return

    def _apply_to_name(self, ctx, itemname, handle, geoname):
        # Try the direct binding dict first.
        direct = self._direct_bindings.get(itemname)
        if direct:
            self._assign_to_geo(ctx, direct, handle, geoname)
            return True
        # Go through patterns.
        for p in self._pattern_bindings:
            if fnmatch.fnmatchcase(itemname, p[0]):
                self._assign_to_geo(ctx, p[1], handle, geoname)
                return True
        return False

    def _assign_to_geo(self, ctx, material, handle, geoname):
        ctx.Connect(material, '', handle, 'geometryattributes')
        # Set the texture names on the geo
        attrs = {}
        for tex in self._textures[material]:
            attrs[tex[0]] = str(tex[1] + geoname + '.ptx')
        if attrs:
            ctx.SetAttribute(handle, **attrs)

    def add_assignment(self, pattern, material_handle, textures):
        # TODO check for conflicting duplicate assignments
        if '*' in pattern:
            self._pattern_bindings.append((pattern, material_handle))
        else:
            self._direct_bindings[pattern] = material_handle
        self._textures[material_handle] = textures

class SceneExporter:

    def __init__(self, args, ctx):
        """
        args : The parsed script argument.
        ctx : The NSI.Context context to export to.
        """
        self._args = args
        self._ctx = ctx
        # This set is used to record which archives (.obj) we've read so we
        # don't read them more than once. Some elements have multiple instances
        # with different item placements but all based off the same .obj
        # archives (eg. two tree instances might have different leaf placement
        # but the actual leaf geo would be shared between trees).
        #
        # For an example of this, see isCoral. Another interesting case is
        # isHibiscusYoung which uses the obj files from isHibiscus. Which is
        # why this set is global.
        #
        # Note that the element geometry also goes through this system even
        # though it does not really need to as we know how to load it only
        # once. But it's mostly harmless and avoids two ways to load obj.
        self._loaded_obj = set()
        # List of exported cameras.
        self._exported_cameras = []

    def _absolute_path(self, path):
        return os.path.join(self._args.scene_path, path)

    def _load_json(self, path):
        with open(self._absolute_path(path), 'r') as f:
            json_data = json.load(f)
        return json_data

    def _script_shader_path(self, shader):
        return str(os.path.join(
            os.path.dirname(os.path.abspath(__file__)), shader))

    def _obj_reader_path(self):
        return str(os.path.join(
            os.path.dirname(os.path.abspath(__file__)), 'obj_reader.so'))

    def _nsi_archive_path(self, jsonpath):
        p = os.path.join(self._args.tmppath, os.path.basename(jsonpath[:-5]) + '.nsi')
        return (str(p), os.access(p, os.R_OK))

    def _read_nsi_archive(self, archivePath):
        self._ctx.Evaluate(type='apistream', filename=archivePath)

    def outputCamera(self, cameraFile):
        """
        Outputs a camera to NSI.

        cameraFile: Relative (to scene) path to json file with camera data.
        """
        camData = self._load_json(cameraFile)
        # camData['name'] is wrong for some cameras so use the file name.
        m = re.search('/(\w+)\.json', cameraFile)
        if not m:
            return
        camHandle = m.group(1)
        self._exported_cameras.append(camHandle)

        # Create camera node.
        self._ctx.Create(camHandle, 'perspectivecamera')
        self._ctx.SetAttribute(camHandle, fov=nsi.FloatArg(camData['fov']))
        # TODO: Set depth of field.

        # Create transform node and connect to camera and root.
        camTrsHandle = camHandle + '_transform'
        self._ctx.Create(camTrsHandle, 'transform')
        self._ctx.Connect(camHandle, '', camTrsHandle, 'objects')
        self._ctx.Connect(camTrsHandle, '', nsi.SCENE_ROOT, 'objects')

        # Create a screen node and connect to camera.
        camScreenHandle = camHandle + '_screen'
        self._ctx.Create(camScreenHandle, 'screen')
        self._ctx.Connect(camScreenHandle, '', camHandle, 'screens')
        self._ctx.SetAttribute(camScreenHandle, oversampling=16)

        sw = camData['screenwindow']
        hres = float(self._args.hres)
        vres = hres / camData['ratio']
        self._ctx.SetAttribute(camScreenHandle,
            screenwindow=nsi.Arg(
                (sw[0], sw[2], sw[1], sw[3]), type=nsi.Type.Double, arraylength=2),
            resolution=nsi.Arg((int(hres), int(vres)), arraylength=2))

        # Compute camera transformation matrix.
        eye = camData['eye']
        look = camData['look']
        up = camData['up']
        camZ = vec_normalize(vec_sub(eye, look))
        camX = vec_normalize(vec_cross(up, camZ))
        camY = vec_normalize(vec_cross(camZ, camX))

        camMatrix = (
            camX[0], camX[1], camX[2], 0,
            camY[0], camY[1], camY[2], 0,
            camZ[0], camZ[1], camZ[2], 0,
            eye[0], eye[1], eye[2], 1)

        self._ctx.SetAttribute(camTrsHandle, transformationmatrix=nsi.Arg(
            camMatrix, type=nsi.Type.DoubleMatrix))

    def outputAllCameras(self, camerasDir):
        """
        Outputs all cameras from a directory to NSI.

        camerasDir: Relative path to directory which contains camera definition files.
        """
        for filename in os.listdir(self._absolute_path(camerasDir)):
            filepath = os.path.join(camerasDir, filename)
            if os.path.isfile(self._absolute_path(filepath)):
                self.outputCamera(filepath)

    def outputQuadLight(self, lightName, lightData):
        xformHandle = lightName + "_transform"
        geoHandle = lightName + "_geo"
        attrHandle = lightName + "_attributes"
        shaderHandle = lightName + "_shader"

        self._ctx.Create(xformHandle, 'transform')
        self._ctx.Connect(xformHandle, '', nsi.SCENE_ROOT, 'objects')
        self._ctx.SetAttribute(xformHandle, transformationmatrix=nsi.Arg(
            lightData['translationMatrix'], type=nsi.Type.DoubleMatrix))

        self._ctx.Create(geoHandle, 'mesh')
        self._ctx.Connect(geoHandle, '', xformHandle, 'objects')
        w = lightData['width']
        h = lightData['height']
        self._ctx.SetAttribute(geoHandle,
            nvertices=4,
            #P=nsi.Arg((-w/2, -h/2, 0, w/2, -h/2, 0, w/2, h/2, 0, -w/2, h/2, 0),
            # This seems to be the desired orientation.
            P=nsi.Arg((-w/2, -h/2, 0, -w/2, h/2, 0, w/2, h/2, 0, w/2, -h/2, 0),
                type=nsi.Type.Point))

        self._ctx.Create(attrHandle, 'attributes')
        self._ctx.Connect(attrHandle, '', geoHandle, 'geometryattributes')
        self._ctx.SetAttribute(attrHandle, **{'visibility.camera':0})

        # Light linking I made up from the README description.
        linkTable = {}
        if self._args.lightlink:
            linkTable = {'palm_': 'isPalmRig', 'beach_': 'isBeach'}
        for pattern, el in linkTable.items():
            if re.search(pattern, lightName, re.IGNORECASE):
                self._ctx.SetAttribute(attrHandle, visibility=0)
                self._ctx.Create(el + '_attributes', 'attributes')
                self._ctx.Connect(el + '_attributes', '', attrHandle, 'visibility', value=1)

        self._ctx.Create(shaderHandle, 'shader')
        self._ctx.Connect(shaderHandle, '', attrHandle, 'surfaceshader')
        lc = lightData['color']
        self._ctx.SetAttribute(shaderHandle,
            shaderfilename=self._script_shader_path('moana_quadlight'),
            i_color=nsi.ColorArg(lc[0], lc[1], lc[2]),
            exposure=nsi.FloatArg(lightData['exposure']))

    def outputDomeLight(self, lightName, lightData):
        xformHandle = lightName + "_transform"
        geoHandle = lightName + "_geo"
        attrHandle = lightName + "_attributes"
        shaderHandle = lightName + "_shader"

        self._ctx.Create(xformHandle, 'transform')
        self._ctx.Connect(xformHandle, '', nsi.SCENE_ROOT, 'objects')
        self._ctx.SetAttribute(xformHandle, transformationmatrix=nsi.Arg(
            lightData['translationMatrix'], type=nsi.Type.DoubleMatrix))

        self._ctx.Create(geoHandle, 'environment')
        self._ctx.Connect(geoHandle, '', xformHandle, 'objects')

        self._ctx.Create(attrHandle, 'attributes')
        self._ctx.Connect(attrHandle, '', geoHandle, 'geometryattributes')
        if self._args.nosky:
            self._ctx.SetAttribute(attrHandle, **{'visibility.camera':0})

        self._ctx.Create(shaderHandle, 'shader')
        self._ctx.Connect(shaderHandle, '', attrHandle, 'surfaceshader')
        lc = lightData['color']
        # For some reason, these paths include one directory level too many.
        lenvmap = '../' + lightData['map']
        lenvmapCamera = '../' + lightData['envmapCamera']
        self._ctx.SetAttribute(shaderHandle,
            shaderfilename=self._script_shader_path('moana_dome.oso'),
            i_color=nsi.ColorArg(lc[0], lc[1], lc[2]),
            exposure=nsi.FloatArg(lightData['exposure']),
            envmap=str(self._absolute_path(lenvmap)),
            envmapCamera=str(self._absolute_path(lenvmapCamera)))
        # Enable automatic tdl generation.
        self._ctx.SetAttribute(shaderHandle, **{
            'envmap.meta.colorspace':'auto',
            'envmapCamera.meta.colorspace':'auto'})

    def outputLights(self, lightsFile):
        """
        Output all the lights from a json file.
        """
        lightsData = self._load_json(lightsFile)
        for lightName, lightDef in lightsData.items():
            if lightDef['type'] == 'quad':
                self.outputQuadLight(lightName, lightDef)
            if lightDef['type'] == 'dome':
                self.outputDomeLight(lightName, lightDef)

    def outputLayers(self, cameraNames):
        """
        Outputs the outputlayer nodes for all requested cameras

        cameraNames: The cameras to render with. 'all' means all of them.
        Returns a dict which maps camera names to layer handles.
        """
        if 'all' in cameraNames:
            cameraNames = self._exported_cameras

        layers = {}
        key = 0;
        for cameraName in self._exported_cameras:
            if not cameraNames or cameraName in cameraNames:
                screenHandle = cameraName + '_screen'
                layerHandle = cameraName + '_beauty'

                self._ctx.Create(layerHandle, 'outputlayer')
                self._ctx.Connect(layerHandle, '', screenHandle, 'outputlayers')
                self._ctx.SetAttribute(layerHandle,
                    variablename='Ci',
                    scalarformat='half',
                    layername=cameraName,
                    withalpha=1,
                    sortkey=key)

                layers[cameraName] = layerHandle
                key += 1
        return layers

    def outputDisplay(self, layers, driver):
        """
        Outputs a display driver for 3Delight Display or an exr file.
        """
        for cameraName, layerHandle in layers.items():
            driverHandle = driver + '_' + cameraName + '_driver'
            self._ctx.Create(driverHandle, 'outputdriver')
            self._ctx.SetAttribute(driverHandle,
                drivername=driver,
                imagefilename=('island_' + cameraName + '.exr'))
            self._ctx.Connect(driverHandle, '', layerHandle, 'outputdrivers')

    def outputOneMaterial(self, elementName, matName, matData):
        """
        Output the required shader nodes for one material.
        """
        # First, an attributes node which we use for assignment. Some material
        # names are reused between scene elements (eg. there is more than one
        # 'trunk') so we prefix the material name with the element name.
        matHandle = elementName + '_' + matName
        self._ctx.Create(matHandle, 'attributes')
        # Build texture list for binder. It will resolve the final texture
        # names when binding as there is a specific texture for every piece of
        # geometry the material is bound to.
        colorMap = matData.get('colorMap', '')
        dispMap = matData.get('displacementMap', '')
        textures = []
        if colorMap != '':
            textures.append(('colorMap',
                str(self._absolute_path(colorMap)) + '/'))
        if dispMap != '':
            textures.append(('dispMap',
                str(self._absolute_path(dispMap)) + '/'))
        if self._args.notextures:
            textures.clear()
        # Record the assignments for later use
        for a in matData['assignment']:
            self._material_binder.add_assignment(a, matHandle, textures)
        # Special case for the hidden material which really means don't render.
        if matName == 'hidden':
            self._ctx.SetAttribute(matHandle, visibility=0)
            return
        # Output the shaders.
        matSurfaceHandle = matHandle + '_surface'
        self._ctx.Create(matSurfaceHandle, 'shader')
        self._ctx.Connect(matSurfaceHandle, '', matHandle, 'surfaceshader')
        self._ctx.SetAttribute(matSurfaceHandle,
            shaderfilename=self._script_shader_path('moana_surface'))
        matColor = matData.get('baseColor', (1, 1, 1, 1))
        surfaceArgs = {}
        surfaceArgs['baseColor'] = nsi.ColorArg(
            matColor[0], matColor[1], matColor[2])
        # All float/int values are passed through.
        for name, value in matData.items():
            if isinstance(value, (float, int)):
                surfaceArgs[name] = nsi.FloatArg(value)
        self._ctx.SetAttribute(matSurfaceHandle, **surfaceArgs)
        # See if we should add a displacement shader.
        if self._args.displace and dispMap != '':
            matDispHandle = matHandle + '_displacement'
            self._ctx.Create(matDispHandle, 'shader')
            self._ctx.Connect(matDispHandle, '', matHandle, 'displacementshader')
            self._ctx.SetAttribute(matDispHandle,
                shaderfilename=self._script_shader_path('moana_displacement'))
            self._ctx.SetAttribute(matHandle, displacementbound=1.0)
        # Because we don't render caustics, make water only visible to camera.
        if matData.get('refractive', 0) != 0:
            self._ctx.SetAttribute(matHandle,
                **{'visibility':0, 'visibility.camera':1})

    def outputElementMaterials(self, elementName, matFile):
        matData = self._load_json(matFile)
        for key, value in matData.items():
            self.outputOneMaterial(elementName, key, value)

    def outputObjMaterialBindings(self, objFile, prefix):
        m = re.match('(.*)\.obj', objFile)
        if not m:
            return
        hierfile = m.group(1) + '.hier'
        hierData = self._load_json(hierfile)
        # Patch the ocean (has incorrect geo name in .hier file).
        if 'water1_geo' in hierData:
            hierData['ocean_geo'] = ''
            hierData['ocean_geo1'] = ''
        # Apply the material bindings.
        for geo, path in hierData.items():
            self._material_binder.apply(self._ctx, geo, path, prefix + geo)

    def outputElementObj(self, objFile):
        """
        Outputs the geometry from a .obj file.

        The geometry is all grouped under a transform with objFile used as a
        handle.

        objFile is also used a handle prefix to all the geo inside the .obj as
        some obj in the scene share identifiers (eg. all the isPalmRig and
        isPalmDead obj have a trunk0001_geo element)
        """
        if objFile in self._loaded_obj:
            return
        # Unlike all other geo, the ocean is not meant to be rendered as subdiv.
        do_subdiv=0
        if self._args.subdiv and not re.search('osOcean', objFile):
            do_subdiv=1
        self._loaded_obj.add(objFile)
        self._ctx.Create(objFile, 'transform')
        self._ctx.Evaluate(
            type='dynamiclibrary',
            filename=self._obj_reader_path(),
            obj_file=str(self._absolute_path(objFile)),
            parentnode=str(objFile),
            prefix=str(objFile + '|'),
            subdiv=do_subdiv)
        self.outputObjMaterialBindings(objFile, objFile + '|')

    def outputInstancedArchive(self, archiveDef, instanceName, primName, primHandle):
        (archivePath, archiveExists) = self._nsi_archive_path(archiveDef['jsonFile'])
        print('-> ' + instanceName + ' -> ' + primName + ' -> instanced primitives: ' + archiveDef['jsonFile']
            + (' (reusing conversion)' if archiveExists else ' (converting)'))
        archives = archiveDef['archives']
        # Load the obj if not already done somewhere else.
        for obj in archives:
            self.outputElementObj(obj)
            # Produce the instancing (or reuse from previous conversion run).
        if not archiveExists:
            instancingData = self._load_json(archiveDef['jsonFile'])
            ctx = nsi.Context()
            ctx.Begin(streamfilename=archivePath, streamformat='binarynsi')
            # Use a separate instancer per obj. It's just simpler.
            for obj in archives:
                # Setup the instancer.
                handle = instanceName + '_' + primName + '_' + obj + '_instancer'
                ctx.Create(handle, 'instances')
                ctx.Connect(handle, '', primHandle, 'objects')
                ctx.Connect(obj, '', handle, 'sourcemodels')
                # Gather matrices of all the instances of that obj.
                matrices = []
                instances = instancingData[obj]
                for name, xform in instances.items():
                    matrices.extend(xform)
                # Output the matrices.
                ctx.SetAttribute(handle, transformationmatrices=nsi.Arg(
                    matrices, type=nsi.Type.DoubleMatrix))
            ctx.End()
        self._read_nsi_archive(archivePath)

    def outputInstancedVariants(self, archiveDef, instanceName, primName, primHandle):
        print('-> ' + instanceName + ' -> ' + primName + ' -> instanced variants: ' + archiveDef['jsonFile'])
        variants = archiveDef['variants']
        instancingData = self._load_json(archiveDef['jsonFile'])
        element = archiveDef['element']
        # Use a separate instancer per variant. It's just simpler.
        for variant in variants:
            # Figure out variant handle. This is groupHandle in outputElementInstance.
            variantHandle = element + '_group_' + variant
            # Create a transform node for the variant in case it has not been exported yet.
            self._ctx.Create(variantHandle, 'transform')
            # Setup the instancer.
            handle = instanceName + '_' + primName + '_' + variant + '_instancer'
            self._ctx.Create(handle, 'instances')
            self._ctx.Connect(handle, '', primHandle, 'objects')
            self._ctx.Connect(variantHandle, '', handle, 'sourcemodels')
            # Gather matrices of all the instances of that variant.
            matrices = []
            instances = instancingData[variant]
            for name, xform in instances.items():
                matrices.extend(xform)
            # Output the matrices.
            self._ctx.SetAttribute(handle, transformationmatrices=nsi.Arg(
                matrices, type=nsi.Type.DoubleMatrix))

    def outputInstancedCurves(self, curveDef, instanceName, primName, primHandle):
        (archivePath, archiveExists) = self._nsi_archive_path(curveDef['jsonFile'])
        print('-> ' + instanceName + ' -> ' + primName + ' -> instanced curves: ' + curveDef['jsonFile']
            + (' (reusing conversion)' if archiveExists else ' (converting)'))
        if not archiveExists:
            curveData = self._load_json(curveDef['jsonFile'])
            curvesHandle = instanceName + '_' + primName + '_geo'
            ctx = nsi.Context()
            ctx.Begin(streamfilename=archivePath, streamformat='binarynsi')
            # Create the curves node.
            ctx.Create(curvesHandle, 'curves')
            ctx.Connect(curvesHandle, '', primHandle, 'objects')
            # Assign material.
            self._material_binder.apply(ctx, primName, '', curvesHandle)
            # Set number of vertices per curve.
            nv=len(curveData[0])
            ctx.SetAttribute(curvesHandle, nvertices=nv, extrapolate=1)
            # Compute vertex width for one curve.
            rootwidth = float(curveDef['widthRoot'])
            tipwidth = float(curveDef['widthTip'])
            width=[rootwidth + (i / float(nv-1)) * (tipwidth - rootwidth) for i in range(nv)]
            ctx.SetAttribute(curvesHandle,
                width=nsi.Arg(width, type=nsi.Type.Float, flags=nsi.Flags.PerVertex))
            flat_vertices=[f for curve in curveData for vertex in curve for f in vertex]
            ctx.SetAttribute(curvesHandle,
                P=nsi.Arg(flat_vertices, type=nsi.Type.Point))
            ctx.End()
        self._read_nsi_archive(archivePath)

    def outputInstancedPrimitives(self, instanceName, primitives, groupHandle):
        """
        Outputs instanced primitives. This can be either geometry from an .obj
        which is instanced in multiple copies or curve defined directly in the
        json.

        groupHandle: The transform name under which we should group the output. We
        also create it.
        """
        self._ctx.Create(groupHandle, 'transform')
        for primName, primData in primitives.items():
            primHandle = instanceName + '_' + primName + '_iprim_group'
            self._ctx.Create(primHandle, 'transform')
            self._ctx.Connect(primHandle, '', groupHandle, 'objects')
            if primData['type'] == 'archive':
                self.outputInstancedArchive(primData, instanceName, primName, primHandle)
            if primData['type'] == 'curve':
                self.outputInstancedCurves(primData, instanceName, primName, primHandle)
            if primData['type'] == 'element':
                self.outputInstancedVariants(primData, instanceName, primName, primHandle)

    def outputElementInstance(self, masterData, instanceData):
        """
        Outputs an instance of an element. This can be either the master copy,
        an instance of it (with or without instance specific geometry) or a
        variant (an unused group to be instanced by a different element).

        masterData: Data for the top level element instance. This is needed when a
        secondary instance inherits some of its things. If None then we're the base
        object.
        instanceData: Dictionary with data for this instance. There might be extra
        data too as we pass the whole thing for the main instance.
        """
        instanceName = instanceData['name']
        # An intermediate transform node which groups all geo, untransformed.
        groupHandle = instanceName + '_group_' + instanceData.get('variant', 'base')
        # Make sure this is unique too. And make it nice because it's printed.
        if 'variant' in instanceData:
            instanceName += ' (' + instanceData['variant'] + ' variant)'

        self._ctx.Create(groupHandle, 'transform')
        xform = instanceData.get('transformMatrix')
        # If there's no transformMatrix, this is just a variant which will
        # actually be instanced elsewhere. So don't connect it.
        if xform:
            # The transform node to actually place the instance in the scene.
            instanceHandle = instanceName + '_instance'
            self._ctx.Create(instanceHandle, 'transform')
            self._ctx.Connect(instanceHandle, '', nsi.SCENE_ROOT, 'objects')
            self._ctx.Connect(groupHandle, '', instanceHandle, 'objects')
            self._ctx.SetAttribute(instanceHandle, transformationmatrix=nsi.Arg(
                xform, type=nsi.Type.DoubleMatrix))
            # Attributes node for the whole element, exported for each instance.
            # It's ok to duplicate creation. Used for crude light linking.
            elementName = masterData['name'] if masterData else instanceName
            self._ctx.Create(elementName + '_attributes', 'attributes')
            self._ctx.Connect(elementName + '_attributes', '', instanceHandle, 'geometryattributes')

        elementObjFile = instanceData.get('geomObjFile')
        if elementObjFile:
            # Output our own geometry.
            print('-> ' + instanceName + ' -> geo: ' + elementObjFile)
            self.outputElementObj(elementObjFile)
            geoHandle = elementObjFile;
        else:
            # Reuse geometry from master copy.
            print('-> ' + instanceName + ' -> reusing ' + masterData['name'] + ' geo')
            geoHandle = masterData['geomObjFile']
        self._ctx.Connect(geoHandle, '', groupHandle, 'objects')

        # Output instanced primitives, if needed.
        instancedPrimitives = instanceData.get('instancedPrimitiveJsonFiles')
        # Some secondary element copies have no instanced primitive table and
        # some have an empty one. We must still check the master instance even
        # in the empty table case.
        if instancedPrimitives is not None and len(instancedPrimitives) > 0:
            # Output our own instanced primitives.
            print('-> ' + instanceName + ' -> specific instanced primitives')
            iP_handle = instanceName + '_instanced_primitives_group'
            self.outputInstancedPrimitives(instanceName, instancedPrimitives, iP_handle)
            self._ctx.Connect(iP_handle, '', groupHandle, 'objects')
        elif masterData is not None and 'instancedPrimitiveJsonFiles' in masterData:
            # Reuse the master instance's instanced primitives.
            print('-> ' + instanceName + ' -> reusing ' + masterData['name'] + ' instanced primitives')
            iP_handle = masterData['name'] + '_instanced_primitives_group'
            self._ctx.Connect(iP_handle, '', groupHandle, 'objects')

    def outputElement(self, element):
        """
        Outputs one element of the scene. An element in this scene is a top level
        logical thing.
        """
        elementData = self._load_json(os.path.join('json', element, element + '.json'))
        elementName = elementData['name']
        print('-> ' + elementName)
        if elementName != element:
            raise Exception("Element name mismatch")

        elementMatFile = elementData['matFile']
        instancedCopies = elementData.get('instancedCopies')
        variants = elementData.get('variants')

        # New object to record material assignments.
        self._material_binder = MaterialBinder()

        # Output materials.
        self.outputElementMaterials(elementName, elementMatFile)

        # Output the main instance.
        self.outputElementInstance(None, elementData)
        # Output any extra copies.
        if instancedCopies:
            for key, instanceData in instancedCopies.items():
                self.outputElementInstance(elementData, instanceData)
        # Output any variants. These end up instanced elsewhere.
        if variants:
            for key, variantData in variants.items():
                # Cheat a little and make this look like an instance.
                variantData['name'] = elementName
                variantData['variant'] = key
                self.outputElementInstance(elementData, variantData)
        # Done with these.
        self._material_binder = None

    def outputAllElements(self):
        """
        Outputs all required elements of the scene.
        """
        element_filter = set(self._args.elements) if self._args.elements else None
        for filename in os.listdir(self._absolute_path('obj')):
            if not element_filter or filename in element_filter:
                self.outputElement(filename)

# Main script starts here.

argparser = argparse.ArgumentParser(description='Moana scene NSI conversion script')

argparser.add_argument('--scene', dest='scene_path', metavar='PATH', default='.', help='Path to scene data.')
argparser.add_argument('--resolution', dest='hres', metavar='RES', default=2048, help='Horizontal resolution.')
argparser.add_argument('--element', dest='elements', metavar='NAME', action='append', help='Element to render. Can be used multiple times.')
argparser.add_argument('--no-textures', dest='notextures', action='store_true', help='Disable textures.')
argparser.add_argument('--archive', dest='nsifile', metavar='FILENAME', help='Produce an archive instead of rendering.')
argparser.add_argument('--camera', dest='cameras', metavar='CAMERA', action='append', help='Name of a camera to render or all. Can be used multiple times.')
argparser.add_argument('--ascii', dest='ascii', action='store_true', help='Produce an ASCII archive instead of binary.')
argparser.add_argument('--no-sky', dest='nosky', action='store_true', help='Make the dome light invisible to camera.')
argparser.add_argument('--no-light-link', dest='lightlink', action='store_false', help='Disable light linking.')
argparser.add_argument('--subdiv', dest='subdiv', action='store_true', help='Render models as subdivision surfaces.')
argparser.add_argument('--displacement', dest='displace', action='store_true', help='Render with displacement maps.')
argparser.add_argument('--exr', dest='writeexr', action='store_true', help='Produce exr files.')
argparser.add_argument('--tmp', dest='tmppath', metavar='TMPPATH', help='Path where to put temporary files of the converter. They allow reuse of parts of the conversion process which are extremely slow because of huge json files. By default, a nsi directory is created below the scene path.')

args = argparser.parse_args()

if not args.tmppath:
    args.tmppath = os.path.join(args.scene_path, 'nsi')
    if not os.access(args.tmppath, os.F_OK):
        os.mkdir(args.tmppath)
    print('Using ' + args.tmppath + ' to store some conversion files.')

begin_args = {}
if args.nsifile:
    begin_args['streamfilename'] = args.nsifile

if not args.ascii:
    begin_args['streamformat'] = 'binarynsi'

ctx = nsi.Context()
ctx.Begin( **begin_args )

exporter = SceneExporter(args, ctx)
exporter.outputAllCameras('json/cameras')
exporter.outputAllElements()
exporter.outputLights('json/lights/lights.json')

layers = exporter.outputLayers(args.cameras or ['shotCam'])
exporter.outputDisplay(layers, 'idisplay')
if args.writeexr:
    exporter.outputDisplay(layers, 'exr')

ctx.SetAttribute(nsi.SCENE_GLOBAL, **{'quality.shadingsamples':64})

print('Python scene conversion finished.')

ctx.RenderControl(action='start')
if not args.nsifile:
    ctx.RenderControl(action='stop')
ctx.End()

# vim: set softtabstop=4 expandtab shiftwidth=4:
