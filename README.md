# Moana to NSI Conversion Script

This short Python script converts the [Moana dataset](https://www.technology.disneyanimation.com/islandscene)
into an `.nsi` file that can be rendered directly using `renderdl`. The Python script
uses the Python NSI API to create the `.nsi` file. As a demonstation of how to
us NSI procedurals, we chose to write a procedural `.obj` reader that will load
the assets as-is during render time.

Features:

* Subdivision and displacements rendering (to sub-pixel accuracy)
* Ptex rendering
* Multi-camera rendering: rendering all of the cameras at once
* OSL shaders
* Light-linking

```
usage: convertNSI.py [-h] [--scene PATH] [--resolution RES] [--element NAME]
                     [--no-textures] [--archive FILENAME] [--camera CAMERA]
                     [--ascii] [--no-sky] [--no-light-link] [--subdiv]
                     [--displacement] [--exr] [--tmp TMPPATH]

Moana scene NSI conversion script

optional arguments:
  -h, --help          show this help message and exit
  --scene PATH        Path to scene data.
  --resolution RES    Horizontal resolution.
  --element NAME      Element to render. Can be used multiple times.
  --no-textures       Disable textures.
  --archive FILENAME  Produce an archive instead of rendering.
  --camera CAMERA     Name of a camera to render or all. Can be used multiple
                      times.
  --ascii             Produce an ASCII archive instead of binary.
  --no-sky            Make the dome light invisible to camera.
  --no-light-link     Disable light linking.
  --subdiv            Render models as subdivision surfaces.
  --displacement      Render with displacement maps.
  --exr               Produce exr files.
  --tmp TMPPATH       Path where to put temporary files of the converter. They
                      allow reuse of parts of the conversion process which are
                      extremely slow because of huge json files. By default, a
                      nsi directory is created below the scene path.
```

![beachCam](/uploads/01a3a3395745fb36f7e32358792a6fa3/beachCam.jpg)
![shotCam](/uploads/cdcdb6f710630248521d75e07368a0c2/shotCam.jpg)
