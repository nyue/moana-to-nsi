#include "nsi_procedural.h"
#include "nsi.hpp"
#include "nsi_dynamic.hpp"

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <vector>

struct ObjProcedural : public NSIProcedural_t
{
	ObjProcedural(const char* nsi_library_path)
		:	api(nsi_library_path)
	{
	}

	NSI::DynamicAPI api;
};


static void SetMeshAttributes(
	NSI::Context& i_nsi,
	const char* i_handle,
	const char* i_name,
	bool i_subdiv,
	const std::vector<float>& i_P,
	const std::vector<float>& i_N,
	const std::vector<int>& i_P_indices,
	const std::vector<int>& i_N_indices,
	const std::vector<int>& i_nvertices)
{
	i_nsi.SetAttribute(i_handle,
		(
			NSI::StringArg("name", i_name),
			NSI::IntegersArg("nvertices", &i_nvertices[0], i_nvertices.size()),
			NSI::IntegersArg("P.indices", &i_P_indices[0], i_P_indices.size()),
			NSI::PointsArg("P", &i_P[0], i_P.size()/3)
		) );

	if(i_subdiv)
	{
		i_nsi.SetAttribute(
			i_handle, NSI::StringArg("subdivision.scheme", "catmull-clark"));
	}
	else
	{
		i_nsi.SetAttribute(i_handle,
			(
				NSI::IntegersArg("N.indices", &i_N_indices[0], i_N_indices.size()),
				NSI::NormalsArg("N", &i_N[0], i_N.size()/3)
			) );
	}
}	

/* Accept an integer, with optional leading blanks. */
static bool AcceptInt(char *&io_c, int *o_v)
{
	char *c = io_c;
	while (*c == ' ' || *c == '\t')
		++c;
	bool negative = *c == '-';
	c += negative;
	if (!(*c >= '0' && *c <= '9'))
		return false;
	int v = *c - '0';
	++c;
	while (*c >= '0' && *c <= '9')
	{
		v = v * 10 + (*c - '0');
		++c;
	}
	io_c = c;
	*o_v = v;
	return true;
}

/* Accept a 'int//int' vertex entry. */
static bool AcceptVN(char *&io_c, int *v, int *n)
{
	if (!AcceptInt(io_c, v))
		return false;
	if (io_c[0] != '/' || io_c[1] != '/')
		return false;
	io_c += 2;
	return AcceptInt(io_c, n);
}

/*
	Loose float parser. Not perfectly accurate or reliable but fast and
	relatively simple.
*/
static bool AcceptFloat(char *&io_c, float *o_v)
{
	char *c = io_c;
	while (*c == ' ' || *c == '\t')
		++c;
	bool negative = *c == '-';
	c += negative;

	bool have_digits = false;
	double v = 0.0;
	while (*c >= '0' && *c <= '9')
	{
		v = v * 10.0 + double(*c - '0');
		have_digits = true;
		++c;
	}
	if (*c == '.')
	{
		++c;
		double decimal = 0.1;
		while (*c >= '0' && *c <= '9')
		{
			v += decimal * double(*c - '0');
			have_digits = true;
			++c;
			decimal *= 0.1;
		}
	}
	else if (*c == 'e')
	{
		int exponent = 0;
		if (!AcceptInt(c, &exponent))
			return false;
		while (exponent > 0)
		{
			v *= 10;
			--exponent;
		}
		while (exponent < 0)
		{
			v *= 0.1;
			++exponent;
		}
	}

	if (negative)
		v *= -1;

	*o_v = v;
	io_c = c;

	return have_digits;
}

static NSI_PROCEDURAL_EXECUTE(obj_execute)
{
	const NSIParam_t* parent_node = nullptr;
	const NSIParam_t* prefix = nullptr;
	const NSIParam_t* obj_file = nullptr;
	const NSIParam_t* subdiv = nullptr;
	for(int p = 0; p < nparams; p++)
	{
		if(!parent_node && strcmp(params[p].name, "parentnode") == 0)
		{
			parent_node = &params[p];
		}
		if(!prefix && strcmp(params[p].name, "prefix") == 0)
		{
			prefix = &params[p];
		}
		if(!obj_file && strcmp(params[p].name, "obj_file") == 0)
		{
			obj_file = &params[p];
		}
		if(!subdiv && strcmp(params[p].name, "subdiv") == 0)
		{
			subdiv = &params[p];
		}
	}

	/*
		This is the default when the procedural is executed through a procedural
		node instead of a call to NSIEvaluate.
	*/
	const char* parent = NSI_SCENE_ROOT;

	if(parent_node)
	{
		if(parent_node->type != NSITypeString ||
			(parent_node->flags & NSIParamIsArray) != 0 ||
			parent_node->count != 1)
		{
			report(ctx, NSIErrError, "Wrong type for parameter 'parentnode'");
			return;
		}

		const char** p = (const char**)parent_node->data;
		if(!*p)
		{
			report(ctx, NSIErrError, "Invalid parameter 'parentnode'");
			return;
		}

		parent = *p;
	}

	if(!obj_file)
	{
		report(ctx, NSIErrError, "Missing parameter 'obj_file'");
		return;
	}

	if(obj_file->type != NSITypeString ||
		(obj_file->flags & NSIParamIsArray) != 0 ||
		obj_file->count != 1)
	{
		report(ctx, NSIErrError, "Wrong type for parameter 'obj_file'");
		return;
	}

	const char** f = (const char**)obj_file->data;
	if(!*f)
	{
		report(ctx, NSIErrError, "Invalid parameter 'obj_file'");
		return;
	}

	if(prefix &&
		(prefix->type != NSITypeString ||
		(prefix->flags & NSIParamIsArray) != 0 ||
		prefix->count != 1))
	{
		report(ctx, NSIErrError, "Wrong type for parameter 'prefix'");
		return;
	}

	const std::string handle_prefix = prefix ? *(const char**)prefix->data : "";

	if(subdiv &&
		(subdiv->type != NSITypeInteger ||
		(subdiv->flags & NSIParamIsArray) != 0 ||
		subdiv->count != 1))
	{
		report(ctx, NSIErrError, "Wrong type for parameter 'subdiv'");
		return;
	}

	bool is_subdiv = subdiv && *(int*)subdiv->data;

	FILE* obj = fopen(*f, "r");
	if(!obj)
	{
		char buffer[1024];
		snprintf(buffer, sizeof(buffer), "Can't open file '%s'", *f);
		report(ctx, NSIErrError, buffer);
		return;
	}

	// Data accumulators
	std::vector<float> P;
	std::vector<float> N;
	std::vector<int> P_indices;
	std::vector<int> N_indices;
	std::vector<int> nvertices;

	unsigned P_indices_offset = 0;
	unsigned N_indices_offset = 0;

	std::string current_group, current_handle;

	ObjProcedural* oproc = (ObjProcedural*)proc;
	NSI::Context nsi(oproc->api);
	nsi.SetHandle(ctx);

	// Read one line at the time
	int nb_lines = 0;
	char line[2048];
	while(fgets(line, sizeof(line), obj))
	{
		nb_lines++;

		int length = strlen(line);
		while(length > 0 && (isblank(line[length-1]) || iscntrl(line[length-1])))
		{
			length--;
			line[length] = 0;
		}

		char* c = line;
		while(*c && isblank(*c)) c++;

		// Empty line
		if(*c == 0)
		{
			continue;
		}

		// Comment
		if(*c == '#')
		{
//			printf("Comment %s\n", c+1);
			continue;
		}

		// Vertex position
		if(c[0] == 'v' && isblank(c[1]))
		{
			float v[3];
			c += 2;
			if (AcceptFloat(c, v+0) && AcceptFloat(c, v+1) && AcceptFloat(c, v+2))
			{
//				printf("V %f %f %f\n", v[0], v[1], v[2]);
				P.push_back(v[0]);
				P.push_back(v[1]);
				P.push_back(v[2]);
				continue;
			}
		}

		// Vertex normal
		if(c[0] == 'v' && c[1] == 'n' && isblank(c[2]))
		{
			float n[3];
			c += 3;
			if (AcceptFloat(c, n+0) && AcceptFloat(c, n+1) && AcceptFloat(c, n+2))
			{
//				printf("N %f %f %f\n", n[0], n[1], n[2]);
				if(!is_subdiv)
				{
					N.push_back(n[0]);
					N.push_back(n[1]);
					N.push_back(n[2]);
				}
				continue;
			}
		}

		// Face
		if(c[0] == 'f' && isblank(c[1]))
		{
			c += 2;
//			printf("F");

			int nv = 0;
			int v, n, count;
			while(AcceptVN(c, &v, &n))
			{
//				printf(" %d//%d", v, n);

				// Wavefront indices start at 1
				v--;
				n--;

				// Validate non-intersection of groups
				if(v < P_indices_offset)
				{
					sprintf(line, "Vertex index refers to other mesh in file '%s', line %d", *f, nb_lines);
					report(ctx, NSIErrError, line);
					// Skip to the next line (forces level-1 while to continue).
					*c = 0;
					break;
				}
				if(n < N_indices_offset)
				{
					sprintf(line, "Normal index refers to other mesh in file '%s', line %d", *f, nb_lines);
					report(ctx, NSIErrError, line);
					// Skip to the next line (forces level-1 while to continue).
					*c = 0;
					break;
				}

				// Correct indices to take previous groups into account
				v -= P_indices_offset;
				n -= N_indices_offset;

				// Accumulate indices
				P_indices.push_back(v);
				if(!is_subdiv)
				{
					N_indices.push_back(n);
				}

				nv++;
			}
//			printf("\n");

			nvertices.push_back(nv);

			if(!*c)
			{
				continue;
			}
		}

		// Group
		if(c[0] == 'g' && isblank(c[1]))
		{
			c += 1;
			while(*c && isblank(*c)) c++;
//			printf("G %s\n", c);

			std::string new_group = c;
			if(current_group == new_group)
			{
				continue;
			}

			if(!nvertices.empty())
			{
				SetMeshAttributes(
					nsi,
					current_handle.c_str(),
					current_group.c_str(),
					is_subdiv,
					P,
					N,
					P_indices,
					N_indices,
					nvertices);

				// Adjust indices offsets for the next group's faces
				P_indices_offset += P.size()/3;
				N_indices_offset += N.size()/3;

				// Start fresh for the next group
				P.clear();
				N.clear();
				P_indices.clear();
				N_indices.clear();
				nvertices.clear();
			}

			current_group = new_group;
			current_handle = handle_prefix + current_group;

			if(current_group != "default")
			{
				nsi.Create(current_handle, "mesh");
				nsi.Connect(current_handle, "", parent, "objects");
			}

			continue;
		}

		// Material library
		if(strncmp(c, "mtllib", 6) == 0 && isblank(c[6]))
		{
			c += 6;
			while(*c && isblank(*c)) c++;
//			printf("L %s\n", c);
			continue;
		}

		// Material
		if(strncmp(c, "usemtl", 6) == 0)
		{
#if 1
			continue;
#else
			if(isblank(c[6]))
			{
				c += 6;
				while(*c && isblank(*c)) c++;
//				printf("M %s\n", c);
				if(!current_group.empty())
				{
					nsi.Connect(c, "", current_handle, "geometryattributes");
				}
				continue;
			}

			if(c[6] == 0)
			{
				sprintf(
					line,
					"Missing material name in file '%s', line %d",
					*f,
					nb_lines);
				report(ctx, NSIErrWarning, line);
				continue;
			}
#endif
		}

		// Smoothing group
		if(c[0] == 's' && isblank(c[1]))
		{
			continue;
		}

		sprintf(line, "Error reading file '%s', line %d", *f, nb_lines);
		report(ctx, NSIErrError, line);
		break;
	}

	fclose(obj);

	// Handle the last group
	if(!nvertices.empty())
	{
		SetMeshAttributes(
			nsi,
			current_handle.c_str(),
			current_group.c_str(),
			is_subdiv,
			P,
			N,
			P_indices,
			N_indices,
			nvertices);
	}
}


static NSI_PROCEDURAL_UNLOAD(obj_unload)
{
	ObjProcedural* oproc = (ObjProcedural*)proc;
	delete oproc;
}


NSI_PROCEDURAL_LOAD
{
	ObjProcedural* proc = new ObjProcedural(nsi_library_path);
	NSI_PROCEDURAL_INIT(*proc, obj_unload, obj_execute);

	return proc;
}
